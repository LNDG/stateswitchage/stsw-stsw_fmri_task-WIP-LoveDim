Estimate representational BOLD dimensionality

- get beta estimates with SPM
	- save residuals for pre-whitening
- Dimensionality-specific: feature-based regressor
	- BUT: cued/probed?
	- Probed ensures equal trial # in different cue dimensionalities
	--> event-related feature probe regressor covering stimulus presentation
	- alternatively: use 16 combinatorials!
- How to define cross-validation splits?
- spotlight estimation of dimensionality (cf. Ahlheim et al.): only indicate functional dimensionality
- ROI-based approach: estimate degree of dimensionality

---

Alternative approach by Mack et al.:

- single-trial beta estimates for stimulus regressor (~ Mumford et al.)
- PCA dimensionality across single-trial beta estimates
	- separately for each dimensionality condition
	- enter in PLS for multivariate estimation

---

Alternative decoding approach: 

- feature selection based on first-level SPM analysis
- regularized leave-one-out decoding
